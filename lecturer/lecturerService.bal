import ballerinax/kafka;
import ballerina/log;
import ballerina/io;

//subscibes to the topic
kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "school",
    topics: ["lecturer", "student"],

    pollingInterval: 1,
    //allows records to be committed manually, sent manually
    autoCommit: false
};

listener kafka:Listener kafkaListener = check new ("localhost:9092", consumerConfigs);
service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        //records or messages recieved by kafka are processed one by one
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }
        //saves offsets or keys of the returned records by marking them as consumed with a error handler.

        kafka:Error? commitResult = caller->commit();
        //error handler
        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}
//value is in byte(Deserialized value)
function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    
    byte[] value = kafkaRecord.value;
//converts from json to string and prints it out
    string courseOutlineContent = check string:fromBytes(value);
    
    log:printInfo("Received Message: " + courseOutlineContent);

    //calling consumer service function
     io:print(consumerFunction());
    
   
}
    