import ballerinax/kafka;
import ballerina/io;


//subscribes to a topic
kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "group-id",
    offsetReset: "earliest",
    topics: ["lecturer", "student"]

};
kafka:Consumer consumer = check new ("localhost:9092", consumerConfiguration);

public function consumerFunction() returns error? {
    //polls consumer for messages
    kafka:ConsumerRecord[] records = check consumer->poll(1);
    

    foreach var kafkaRecord in records {
       byte[]  courceOutlineContent = kafkaRecord.value;
        //changes from string to bytes(should be in JSON)
        string courseOutline = check string:fromBytes(courceOutlineContent);
        //prints kafka record
        io:println("Received Message: " + courseOutline);
        //stores message as a json
       
        
    }
}