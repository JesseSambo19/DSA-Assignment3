import ballerina/io;
import ballerinax/kafka;

kafka:Producer kafkaProducer = check new ("localhost:9092");

//user creation
type lecturer record {
    string username;
    string lastname;
    string firstname;
    string password;

};
type course record{
    string courseName;
    string courseID;
};


public function insert(any in_type) returns error? {

    io:println(in_type);

    boolean in_lcv = true;
    boolean pref_found = false;
    map<json> lecturer = {
        username: "", 
        lastname: "", 
        firstname: "", 
        password: ""
    };
    map<json> course = {
        courseName: "",
        courseID: ""
    };

    // uuid1String
    string username = "";
    string lastname = "";
    string firstname = "";
    string password = "";
    string courseName = "";
    string courseID = "";
 } //======================================Student record definition=============================================================

   
   
# Description
# + return - Return Value Description  
public function createCourceOutline() returns json{
    
    string coursename = io:readln("Enter course: ");
        string courseRequirements = io:readln("Enter course requirements: ");
        string courseSchedule = io:readln("Enter course schedule: ");
        string assesmentPercentage = io:readln("Enter assesment percentage: ");

        json courseOutline = {"Cource name": coursename,
                              "Requirements":courseRequirements,
                              "Schedule":courseSchedule,
                              "Assesments":assesmentPercentage
                              };
        return courseOutline;                    
}
public function view() returns error?|json{
    kafka:ConsumerRecord[] records = check consumer->poll(3);
    io:println("Waiting for course outline");
    int l = records.length();

    int i = 0;
    foreach var kafkaRecord in records {
        if(i == (0-1)){
            byte[] value = kafkaRecord.value;

            string courseOutline = check string:fromBytes(value);
            io:StringReader sr = new (courseOutline, encoding = "UTF-8");

            json j = check sr.readJson();

            return j;
        }
        
    }

}

public function main() returns error? {
    //create sign in(needs to be created)


   //courses for year 1
   json[] computerScience1 =[{"DSA": createCourceOutline()},
                           {"SDN": createCourceOutline()},
                             {"DTA": createCourceOutline()}                   
                            ];
  //courses for year 2
    json[] computerScience2 = [{"MCI": createCourceOutline()},
                               {"ERP": createCourceOutline()},
                               {"DCE": createCourceOutline()}                   
                            ];
//viewing course outline
    string ans = io:readln("Do you want to view course outline? YES/NO");
    string compAns = ans.toLowerAscii();
    if(ans=="yes"){
        string ansV = io:readln("Which course outline would you like to view. Computer science 1(C1) or Computer science 2(C2) Enter the number in brackets to select");
        string upAnsV = ansV.toUpperAscii();
        if(upAnsV=="C1"){
            io:print(computerScience1.toJsonString());

        }else if(ansV == "C2"){
            io:print(computerScience2.toJsonString());

        //}
    }else {
        io:print("BYE");
    } 
    

    string stringCS1 = computerScience1.toString();
    string stringCS2 = computerScience2.toString();
    io:println(stringCS1);
//Sends Computer Science 1 course outline to topic
    check kafkaProducer->send({
                                topic: "lecturer", 
                                
                                value:  stringCS1.toBytes()
                                
                                });
    io:print();
    //flushes message
    check kafkaProducer->'flush();

//Sends Computer Science 1 course outline to student
    check kafkaProducer->send({
                                topic: "student",
                                
                                value:  stringCS1.toBytes()
                                
                                });
    io:print();
    //flushes message
    check kafkaProducer->'flush();

//Sends Computer Science 2 course outline to lecturer
    check kafkaProducer->send({
                                topic: "lecturer",
                                
                                value:  stringCS2.toBytes()
                                
                                });
    io:print();
    //flushes message
    check kafkaProducer->'flush();

//Sends Computer Science 2 course outline to student
    check kafkaProducer->send({
                                topic: "student",
                                
                                value:  stringCS2.toBytes()
                                
                                });
    io:print();
    //flushes message
    check kafkaProducer->'flush();

    
}
}