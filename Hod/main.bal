import ballerina/io;
import ballerina/http;
import ballerina/uuid;
import ballerinax/kafka;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

type lecturer record {
    string username;
    string lastname;
    string firstname;
    string password;

};


public function insert(any in_type) returns error? {

    io:println(in_type);

    boolean in_lcv = true;
    boolean pref_found = false;
    map<json> Hod = {
        username: "", 
        lastname: "", 
        firstname: "", 
        password: ""
    };

    // uuid1String
    string username = "";
    string lastname = "";
    string firstname = "";
    string password = "";
    //======================================Student record definition=============================================================
}
public function insertInfo() returns json?{
    string lecturerInfo = io:readln("Enter lecturer information: ");
    string courseInfo = io:readln("Enter course information: ");
    string studentInfo = io:readln("Enter student information");
    json[] courseSum = [{"Lecturer Teaching course":lecturerInfo },
                        {"Course Information":courseInfo},
                        {"Student information": studentInfo}
                        ];
    
    return courseSum;

}
    public function main() returns error? {
        json[] courseDetails = [{"DSA":insertInfo(),
                                 "SDN":insertInfo(),
                                 "DTA":insertInfo()
                               }];

        string newCourseDetails = courseDetails.toJsonString();
//sends to Hod
        check kafkaProducer->send({
                                topic: "Hod",
                                value: newCourseDetails.toBytes() });
        check kafkaProducer->'flush();
//sends to student
        check kafkaProducer->send({
                                topic: "student",
                                value: newCourseDetails.toBytes() });
        check kafkaProducer->'flush();

//sends to lecturer
        check kafkaProducer->send({
                                topic: "lecturer",
                                value: newCourseDetails.toBytes() });
        check kafkaProducer->'flush();

}

}




 



    