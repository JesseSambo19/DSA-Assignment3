import ballerina/http;
import ballerinax/kubernetes;

//Three micro services here, whenever I get a request from the book micro service with a book id, it will connect to the book detail micro service
// and look for the matching record in that book id
// If it's mad match it will go to the book review micro service and look for a matching review, if it's available it will go to the book shop

@kubernetes:IstioGateWay {

}

@kubernetes:IstioVirtualService {}
@kubernetes:Service {
    name:"book-shop",
    serviceType:"NodePort"
}
endpoint http:Listener bookShopEP {
    port: 9080
};

endpoint http:Client bookDetailsEP {
    url: "http//book-detail:8080"
};

endpoint http:Client bookReviewEP {
    url: "http//book-review:7070"
};

@kubernetes:Deployment {
    name:"book-shop"
}