import ballerina/http;

endpoint http:Listener bookReviewEP {
    port: 7070
};

@http:ServiceConfig {
    basePath:"/review"
}

service<http:Service> reviewService bind bookReviewEP {
    @http:ResourceConfig {
        methods: ["Get"],
        path: "/{id}"
    }
    getReview (endpoint caller, http:Request request, string id) {
        
    }
}